import speech_recognition as sr
import subprocess
import time

# obtain path to "english.wav" in the same folder as this script
from os import path

class SpeechToText():
	listening = False

	def __init__(self):
		self.r = sr.Recognizer()
		self.m = sr.Microphone()

	def GetTextFromAudioFile(self, filename):
		with sr.AudioFile(filename) as source:
			audio = self.r.record(source)  # read the entire audio file
		return self.r.recognize_sphinx(audio)

	def PromptForAudioFileAndGetText(self):
		audio_file = path.realpath(input("Enter the name of the audio file: "))
		return self.GetTextFromAudioFile(audio_file)

	def _callback(self, recognizer, audio):
		# received audio data, now we'll recognize it
		try:
			speech = recognizer.recognize_sphinx(audio, keyword_entries=[("hello",1.0),("stop",1.0),("list all files",1.0),("shutdown",1.0)]).strip()
			if speech=="":
				return
			print("SPOKEN: \"{}\"\n".format(speech))
			self._checkAndPerformCommand(speech)
		except sr.UnknownValueError:
			print("Does Not Compute")

	def _checkAndPerformCommand(self,command):
		if command not in set(["hello","stop","shutdown"]):
			return
		if command=="stop":
			commandDescription = "EXIT PROGRAM"
			commandFunction = self._stopListening
		elif command=="hello":
			commandDescription = "GREETING"
			commandFunction = self._greeting
		elif command=="shutdown":
			commandDescription = "SHUTTING DOWN"
			commandFunction = self._shutdown
		print("COMMAND RECOGNIZED: {}".format(commandDescription))
		commandFunction()

	def _greeting(self):
		try:
			subprocess.check_output(["say", "Hello. How are you today?"])
		except subprocess.CalledProcessError:
			pass

	def _stopListening(self):
		try:
			subprocess.check_output(["say", "Stopping Program"])
		except subprocess.CalledProcessError:
			pass
		SpeechToText.listening = False	

	def _shutdown(self):
		try:
			subprocess.check_output(["say", "Stopping Program"])
		except subprocess.CalledProcessError:
			pass
		subprocess.call(["shutdown","now"])

	def StartListening(self):
		SpeechToText.listening = True
		with self.m as source:
			self.r.adjust_for_ambient_noise(source)
		self.stop_listening = self.r.listen_in_background(self.m, self._callback)
		print("Listening Until You Say STOP\n")		
		while SpeechToText.listening==True:
			pass
		self.stop_listening(wait_for_stop=False)

def prompt():
	s = SpeechToText()
	speech = s.PromptForAudioFileAndGetText()
	try:
		output = "Sphinx thinks you said \"{}\"".format(speech)				
		print(output)
		subprocess.check_output(["say", output])
	except subprocess.CalledProcessError:
		pass

def listener():
	s = SpeechToText()
	s.StartListening()

listener()